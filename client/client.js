import { AppContainer } from "react-hot-loader"; // AppContainer is a necessary wrapper component for HMR
import React from "react";
import ReactDOM from "react-dom";
import App from "../src/App.jsx";
import { Provider } from "react-redux";

import store from "../src/store";
import { userFetch } from "../src/js/actions/userFetch";
import { statsFetch } from "../src/js/actions/actions";
import { countriesFetch } from "../src/js/actions/countriesFetch";
// initial user and statistical data request
store.dispatch(userFetch())
store.dispatch(statsFetch())
store.dispatch(countriesFetch())

const render = (Component) => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <Component />
      </Provider>
    </AppContainer>,
    document.getElementById("app")
  );
};

render(App);

// Hot Module Replacement
if (module.hot) {
  module.hot.accept("../src/App.jsx", () => {
    render(App)
  });
}
