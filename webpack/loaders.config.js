const path = require('path');
const merge = require('webpack-merge');

const common = {
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: [ 'babel-loader' ],
        exclude: [/node_modules/],
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ],
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'babel-loader'
          },
          {
            loader: 'react-svg-loader',
            query: {
              jsx: true,
              svgo: {
                plugins: [{removeTitle: true}],
                floatPrecision: 5,
                removeDoctype: true,
                removeDesc: true,
                removeUnknownsAndDefaults: true,
                removeUselessStrokeAndFill: true,
                collapseGroups: true,
                convertStyleToAttrs: true,
                removeStyleElement: true,
                cleanupIDs: true
              }
            }
          }
        ],
        include: [
          path.resolve('src'),
          path.resolve('client'),
          path.resolve('semantic'),
        ],
      },
      {
        test: /\.(jpe?g|gif|png|woff2?|ttf|eot)$/,
        include: [
          path.resolve('src'),
          path.resolve('client'),
          path.resolve('semantic'),
        ],
        use: [
          {
            loader: 'file-loader'
          }
        ]
      }
    ]
  },
  resolve: {
    alias: {
      'RelapLogo': path.resolve('client/img/RelapLogoType.svg'),
      'SemanticUI': path.resolve('semantic/dist/semantic.min.css'),
      'Var': path.resolve('client/scss/var.scss'),
      'Base': path.resolve('client/scss/base.scss'),
      'Utils': path.resolve('src/js/utils'),
    }
  }
}

const development = {
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      }
    ]
  }
}

const production = {
}

if (process.env.NODE_ENV === 'development') {
  module.exports = merge(development, common);
} else if (process.env.NODE_ENV === 'production') {
  module.exports = merge(production, common);
} else {
  module.exports = merge(production, common);
}
