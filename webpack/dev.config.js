const { resolve } = require("path");
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  context: resolve(__dirname, "../"),

  entry: [
    "react-hot-loader/patch",
    "./client/client.js",
    "webpack-dev-server/client?http://localhost:3000/",
    "webpack/hot/only-dev-server"
  ],

  devtool: 'eval',

  devServer: {
    hot: true,
    contentBase: resolve(__dirname, "../dist"),
    publicPath: "/",
    host: 'localhost',
    port: 3000
  },

  output: {
    path: resolve(__dirname, "../dist"),
    filename: "bundle.js",
    publicPath: "/"
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.DefinePlugin({
      'ENV': JSON.stringify(process.env.ENV || 'development')
    }),
    new HtmlWebpackPlugin({
      title: "Статистика по рекламе",
      inject: true,
      filename: "index.html",
      template: "./templates/index.html"
    })
  ],
};
