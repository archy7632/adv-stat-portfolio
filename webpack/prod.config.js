const { resolve } = require("path");
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
console.log('process.env.NODE_ENV',process.env.ENV);

module.exports = {
  entry: [
    "./client/client.js",
  ],

  output: {
    path: resolve(__dirname, "../dist"),
    filename: "[name].[hash].js",
    publicPath: "/"
  },

  devtool: 'cheap-module-source-map',

  plugins: [
   new webpack.HotModuleReplacementPlugin(),
   new webpack.NamedModulesPlugin(),
   new webpack.DefinePlugin({
     'ENV': JSON.stringify(process.env.ENV || 'development')
   }),
   new HtmlWebpackPlugin({
     title: "Статистика по рекламе",
     inject: true,
     filename: "index.html",
     template: "./templates/index.html"
   })
 ],

};
