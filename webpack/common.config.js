const { resolve } = require("path");
const webpack = require('webpack');
const merge = require('webpack-merge');

const development = require('./dev.config.js');
const production = require('./prod.config.js');
const loaders = require('./loaders.config.js');

const TARGET = process.env.npm_lifecycle_event;
console.log('___________________TARGET',TARGET);

const common = {};

if (TARGET === 'start' || !TARGET) {
  module.exports = merge(development, common, loaders);
} else if (TARGET === 'b:prod' || !TARGET) {
  module.exports = merge(production, common, loaders);
} else {
  module.exports = merge(development, common, loaders);
}
