import "babel-polyfill";
import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import store from "./store";

import MainChart from "./js/components/MainChart/MainChart.js";
import Footer from "./js/components/Footer/Footer.jsx";
import RangeSelector from "./js/components/RangeSelector/RangeSelector.jsx";
import SummaryTable from "./js/components/SummaryTable/SummaryTable.jsx";
import AppStatusIndicator from "./js/components/AppStatusIndicator/AppStatusIndicator.jsx";
import Toolbar from "./js/components/Toolbar/Toolbar.jsx";
import DownloadBtn from "./js/components/DownloadBtn/DownloadBtn.jsx";
import styles from "../client/scss/base.scss";

import { statsFetch, setFilterDomain, setFilterWidget } from "./js/actions/actions";

import { getDate } from './js/utils';

@connect((store) => {
  return {
    stats: store.stats.statsData,
  };
})
export default class App extends PureComponent {

  handleStatRequest = () => this.props.dispatch( statsFetch() );

  render() {
    const { stats, viewRange, userFetchErrorStatus, requestUrl } = this.props;
    let unauthorised = userFetchErrorStatus === 401;

    return (
      <div className="wrapper">
        <div className="stat-selector-wrapper">
          <Toolbar handleStatRequest={this.handleStatRequest}/>
        </div>
        <div className="flex-parent">
          <div className="sidebar">
            <SummaryTable data={stats.total} />
          </div>
          <div className="content">

            {
              unauthorised
              ? <AppStatusIndicator unauthorised />
              : <MainChart
                  data={stats.period}
                  viewRange={viewRange} />
            }

            {
              unauthorised
              ? null
              : <DownloadBtn
                  name='Выгрузить статистику'
                  url={requestUrl + "&format=csv" } />
            }

          </div>
        </div>
        <Footer/>
      </div>
    );
  }
}
