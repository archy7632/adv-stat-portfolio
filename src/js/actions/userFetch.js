import axios from "axios";
import { dataFetchRequest } from "./actions";

export const USER_FETCH_REQUEST = 'USER_FETCH_REQUEST';
export const USER_FETCH_LOADING = 'USER_FETCH_LOADING';
export const USER_FETCH_FAILURE = 'USER_FETCH_FAILURE';
export const USER_FETCH_SUCCESS = 'USER_FETCH_SUCCESS';

export const SET_USER_DOMAINS = 'SET_USER_DOMAINS';
export const SET_USER_WIDGETS = 'SET_USER_WIDGETS';

export function userFetch() {
  return (dispatch) => {
    let requestDomain = ENV === "development" ? "https://akocharyandev.relap.io" : "";
    let url = "/personal/api/user"
    let fullUrl = requestDomain + url
    dispatch(userFetchLoading(true));
    axios.get(fullUrl, {withCredentials:true})
      .then( response => {

        const userDomains = () => {
          let parsedDomains = {
            null: {
              id: "null",
              domain: "всех доменов",
            }
          };

          response.data.company.domains.map(
            item => parsedDomains[item['id']] = {
              id: item.id,
              domain: item.domain
            }
          );

          return parsedDomains
        }

        const userWidgets = () => {
          let domainsWidgetsList = {}
          response.data.company.domains.map(
            (item, index) => {

              const domainWidgets = response.data.company.domains[index]["widgets"].map(
                (item,index) => ({
                  "id": index + 1,
                  "value": item.id,
                  "text": item.name
                })
              )
              let addDefaultWidget = [
                {
                  id: 0,
                  value: "null",
                  text: 'Все виджеты '
                },
                ...domainWidgets
              ]
              domainsWidgetsList[item.id] = addDefaultWidget
              return domainsWidgetsList
            }
          );

          let widgetsFullList = {
            null: [{
              id: 0,
              value: "null",
              text: 'Все виджеты'
            }],
            ...domainsWidgetsList
          }
          return widgetsFullList;

        }

        console.log('response.data',response.data);
        console.log('userWidgets',userWidgets());
        dispatch( userFetchSuccess(response.data) )
        dispatch( setUserDomains(userDomains()) )
        dispatch( setUserWidgets(userWidgets()) )
      })
      .catch( error => {
        dispatch(userFetchFailure(true, error.response));
        console.log("err",error.response)
      })
  }
}

export function userFetchLoading(bool) {
  return {
    type: USER_FETCH_LOADING,
    isLoading: bool
  }
}

export function userFetchFailure(bool, number) {
  return {
    type: USER_FETCH_FAILURE,
    errLoading: bool,
    errStatus: number
  }
}

export function userFetchSuccess(data) {
  return {
    type: USER_FETCH_SUCCESS,
    isLoggedIn: true,
    user: data
  }
}

export function setUserDomains(data) {
  return {
    type: SET_USER_DOMAINS,
    domains: data
  }
}

export function setUserWidgets(data) {
  return {
    type: SET_USER_WIDGETS,
    widgets: data
  }
}
