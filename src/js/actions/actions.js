import axios from "axios";

import { getDate } from "Utils";

export const STAT_DATA_FETCH_REQUEST = 'STAT_DATA_FETCH_REQUEST';
export const STAT_DATA_FETCH_LOADING = 'STAT_DATA_FETCH_LOADING';
export const STAT_DATA_FETCH_FAILURE = 'STAT_DATA_FETCH_FAILURE';
export const STAT_DATA_FETCH_SUCCESS = 'STAT_DATA_FETCH_SUCCESS';

export const STAT_DATA_FETCH_REQUEST_URL = 'STAT_DATA_FETCH_REQUEST_URL';

export const SET_DOMAIN_ID = 'SET_DOMAIN_ID';
export const SET_WIDGET_ID = 'SET_WIDGET_ID';
export const SET_VIEW_RANGE = 'SET_VIEW_RANGE';
export const SET_VIEW_START_DATE = 'SET_VIEW_START_DATE';
export const SET_VIEW_STOP_DATE = 'SET_VIEW_STOP_DATE';

function isValue(value) {
  let check = value === "null" || value === null || value === "undefined" || value === undefined;
  if (!check) return value;
  return false;
}

export function statsFetch() {
  let domain = ENV === "development" ? "https://akocharyandev.relap.io" : "";
  let url = "/personal/api/stat/monetization";

  return (dispatch, getState) => {
    console.log('getState()',getState());

    let start = "?start=" + getDate( getState().stats.viewRange );
    let stop = "&stop=" + getDate();
    let domain_id = "&domains=" + getState().stats.domain_id;
    let widget_id = "&widgets=" + getState().stats.widget_id;
    let api = isValue(getState().devices.api) ? "&api=" + getState().devices.api : "";
    let is_mobile =  isValue(getState().devices.is_mobile) ? "&is_mobile=" + getState().devices.is_mobile  : "";
    let country =  isValue(getState().countries.countrySelectorValue) ? "&country=" + getState().countries.countrySelectorValue : "";
    let region =  isValue(getState().regions.regionSelectorValue) ? "&region=" + getState().regions.regionSelectorValue : "";

    let fullUrl =
      domain
      + url
      + start
      + stop
      + domain_id
      + widget_id
      + api
      + is_mobile
      + country
      + region;

    dispatch(statsFetchLoading(true));
    axios.get(fullUrl, {withCredentials:true})
      .then(response => {
        console.log('response',response);
        dispatch(statsFetchSuccess(response.data))
      })

      .then(response => {
        dispatch(statsFetchLoading(false));
      })
      .catch( error => {
        dispatch(statsFetchFailure(true, error.response.status));
        dispatch(statsFetchLoading(false));
        console.log(error)
      })
  };
}

export function statsFetchLoading(bool) {
  return {
    type: STAT_DATA_FETCH_LOADING,
    isLoading: bool
  };
}

export function statsFetchFailure(bool, number) {
  return {
    type: STAT_DATA_FETCH_FAILURE,
    errLoading: bool,
    errStatus: number
  }
}

export function statsFetchSuccess(data) {
  return {
    type: STAT_DATA_FETCH_SUCCESS,
    statsData: data
  }
}

export function statsFetchRequestUrl(url) {
  return {
    type: STAT_DATA_FETCH_REQUEST_URL,
    requestUrl: url
  }
}


export function setDomain( string ) {
  return {
    type: SET_DOMAIN_ID,
    domain_id: string
  };
}

export function setWidget( string ) {
  return {
    type: SET_WIDGET_ID,
    widget_id: string
  };
}

export function setViewRange( string ) {
  return {
    type: SET_VIEW_RANGE,
    viewRange: string
  };
}

export function setViewStartDate( string ) {
  return {
    type: SET_VIEW_START_DATE,
    startDate: string
  };
}

export function setViewStopDate( string ) {
  return {
    type: SET_VIEW_STOP_DATE,
    stopDate: string
  };
}
