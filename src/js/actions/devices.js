export const SET_SELECTED_DEVICES = 'SET_SELECTED_DEVICES';

export function setSelectedDevices(api, is_mobile) {
  return {
    type: SET_SELECTED_DEVICES,
    api: api,
    is_mobile: is_mobile
  };
}
