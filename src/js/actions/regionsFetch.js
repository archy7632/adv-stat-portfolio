import axios from "axios";

export const REGIONS_FETCH_REQUEST = 'REGIONS_FETCH_REQUEST';
export const REGIONS_FETCH_LOADING = 'REGIONS_FETCH_LOADING';
export const REGIONS_FETCH_FAILURE = 'REGIONS_FETCH_FAILURE';
export const REGIONS_FETCH_SUCCESS = 'REGIONS_FETCH_SUCCESS';

export const SET_SELECTED_REGION = 'SET_SELECTED_REGION';

export function regionsFetch(code) {
  let domain = ENV === "development" ? "https://akocharyandev.relap.io" : "";
  let url = "/personal/api/regions?country_code=";

  return (dispatch) => {
    dispatch(regionsFetchLoading(true));
    axios.get(domain + url + code, {withCredentials:true})
      .then(response => {
        dispatch(regionsFetchSuccess(response.data))
      })
      .then(response => {
        dispatch(regionsFetchLoading(false));
      })
      .catch( error => {
        dispatch(regionsFetchFailure(true, error.response.status));
        dispatch(regionsFetchLoading(false));
        console.log(error)
      })
  };
}

export function regionsFetchLoading(bool) {
  return {
    type: REGIONS_FETCH_LOADING,
    isLoading: bool
  };
}

export function regionsFetchFailure(bool, number) {
  return {
    type: REGIONS_FETCH_FAILURE,
    errLoading: bool,
    errStatus: number
  }
}

export function regionsFetchSuccess(data) {
  return {
    type: REGIONS_FETCH_SUCCESS,
    regions: data
  }
}

export function setSelectedRegion(string) {
  return {
    type: SET_SELECTED_REGION,
    regionSelectorValue: string
  }
}
