import axios from "axios";

export const COUNTRIES_FETCH_REQUEST = 'COUNTRIES_FETCH_REQUEST';
export const COUNTRIES_FETCH_LOADING = 'COUNTRIES_FETCH_LOADING';
export const COUNTRIES_FETCH_FAILURE = 'COUNTRIES_FETCH_FAILURE';
export const COUNTRIES_FETCH_SUCCESS = 'COUNTRIES_FETCH_SUCCESS';
export const SET_SELECTED_COUNTRY = 'SET_SELECTED_COUNTRY';

export function countriesFetch() {
  let domain = ENV === "development" ? "https://akocharyandev.relap.io" : "";
  let url = "/personal/api/countries";

  return (dispatch) => {
    dispatch(countriesFetchLoading(true));
    axios.get(domain + url, {withCredentials:true})
      .then(response => dispatch(countriesFetchSuccess(response.data)))
      .then(response => {
        dispatch(countriesFetchLoading(false));
      })
      .catch( error => {
        dispatch(countriesFetchFailure(true, error.response.status));
        dispatch(countriesFetchLoading(false));
        console.log(error)
      })
  };
}

export function countriesFetchLoading(bool) {
  return {
    type: COUNTRIES_FETCH_LOADING,
    isLoading: bool
  };
}

export function countriesFetchFailure(bool, number) {
  return {
    type: COUNTRIES_FETCH_FAILURE,
    errLoading: bool,
    errStatus: number
  }
}

export function countriesFetchSuccess(data) {
  return {
    type: COUNTRIES_FETCH_SUCCESS,
    countries: data
  }
}

export function setSelectedCountry(data) {
  return {
    type: SET_SELECTED_COUNTRY,
    countrySelectorValue: data
  }
}
