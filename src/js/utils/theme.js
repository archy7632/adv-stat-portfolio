const defaultIndexes = {
  widget_imps: {
    eng: "WI",
    ru: "",
    color: "#33c4cd",
  },
  widget_views: {
    eng: "Widget Views",
    ru: "Просмотры виджета",
    color: "#33c4cd",
  },
  сlicks: {
    eng: "Clicks",
    ru: "",
    color: "#33c4cd",
  },
  true_ctr: {
    eng: "TrueCTR",
    ru: "Филлрейт",
    color: "#33c4cd",
  },
  
  ad_widget_imps: {
    eng: "AI",
    ru: "",
    color: "#33c4cd",
  },
  ad_widget_views: {
    eng: "Ad Views",
    ru: "Просмотры рекламы",
    color: "#33c4cd",
  },
  ad_widget_view_rate: {
    eng: "Ad VTR",
    ru: "",
    color: "#33c4cd",
  },
  ad_fill_rate: {
    eng: "Fillrate",
    ru: "",
    color: "#33c4cd",
  },
  ecpm: {
    eng: "eCPM",
    ru: "",
    color: "#33c4cd",
  },
  revenue: {
    eng: "Revenue",
    ru: "Доход",
    color: "#33c4cd",
  },
}


export default defaultIndexes
