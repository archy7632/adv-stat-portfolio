export const addZero = (number) => {
  return number < 10 ? '0' + number : number;
};

export const getDate = (viewRange) => {
  let date = new Date();

  let day = date.getDate();
  let month = date.getMonth() + 1;
  let year = date.getFullYear();

  if (viewRange) {
    date.setDate(date.getDate() - viewRange);
    day = date.getDate();
    month = date.getMonth() + 1;
    year = date.getFullYear();
  }
  return year + '-' + addZero( month ) + '-' + addZero( day ) + 'T00:00:00';
};

export const groupNumber = ( num ) => {
    let number = num || 0
    let str = number.toString().split('.');
    if (str[0].length >= 5) {
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1 ');
    }
    if (str[1] && str[1].length >= 5) {
        str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }
    return str === 0 ? null : str.join(',') ;
};

export const formatDateToDDMM = (rawDate) => {
  let date = new Date(rawDate.replace(/\s/, 'T'))
  let day = date.getDate()
  let month = date.getMonth() + 1

  let correctDate = addZero(day) + "." + addZero(month)

  return correctDate
}

export const isInArray = (data, item) => {
  return data.some(value => value === item)
}
