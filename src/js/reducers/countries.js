export function countries( state={
  "countries": [{
    "code": "null",
    "name": { "ru": "Все страны" }
  }],
  "isLoading": null,
  "errLoading": null,
  "errStatus": null,
  "countrySelectorValue": null,
}, action) {
  switch (action.type) {
    case 'COUNTRIES_FETCH_LOADING':
      return {...state, isLoading: action.isLoading};
    case 'COUNTRIES_FETCH_FAILURE':
      return {...state, errLoading: action.errLoading, errStatus: action.errStatus};
    case 'COUNTRIES_FETCH_SUCCESS':
      return {...state, countries: action.countries};
    case 'SET_SELECTED_COUNTRY':
      return {...state, countrySelectorValue: action.countrySelectorValue};
    default:
      return state
  }
}
