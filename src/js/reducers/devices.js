export function devices( state={
  "api": "null",
  "is_mobile": "null"
}, action) {
  switch (action.type) {
    case 'SET_SELECTED_DEVICES':
      return {...state, api: action.api, is_mobile: action.is_mobile};
    default:
      return state
  }
}
