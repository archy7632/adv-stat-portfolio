export function regions( state={
  "regions": [{
    "code": "null",
    "name": { "ru": "Все регионы" }
  }],
  "isLoading": null,
  "errLoading": null,
  "errStatus": null,
  "regionSelectorValue": null
}, action) {
  switch (action.type) {
    case 'REGIONS_FETCH_LOADING':
      return {...state, isLoading: action.isLoading};
    case 'REGIONS_FETCH_FAILURE':
      return {...state, errLoading: action.errLoading, errStatus: action.errStatus};
    case 'REGIONS_FETCH_SUCCESS':
      return {...state, regions: action.regions};
    case 'SET_SELECTED_REGION':
      return {...state, regionSelectorValue: action.regionSelectorValue};
    default:
      return state
  }
}
