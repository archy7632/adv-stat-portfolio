export function user(state = {
  isLoading: false,
  errLoading: false,
  errStatus: null,
  isLoggedIn: false,
  domains: {},
  widgets: {
    null: [{
      id: 0,
      value: "null",
      text: 'Все виджеты '
    }]
  },
  user: {
    user: {
      id: null,
      email: null,
      domains: [
        {
          domain: null,
          widgets:[]
        },
      ],
      companies: []
    },
    company: {
      rights: {},
      id:"",
      domains: [
        {
          balance: null,
          monetizing: true,
          is_active: true,
          widgets: [
             {
                name: "",
                is_enabled: null,
                id: "",
                preset_name: ""
             },
          ],
          domain: "",
          id: ""
        }
      ],
      balance: null,
      name: ""
    }
  }

}, action) {
  switch (action.type) {
    case 'USER_FETCH_LOADING':
      return {...state, isLoading: action.isLoading};
    case 'USER_FETCH_FAILURE':
      return {...state, errLoading: action.errLoading, errStatus: action.errStatus};
    case 'USER_FETCH_SUCCESS':
      return {...state, user: action.user, isLoggedIn: action.isLoggedIn};
    case 'SET_USER_DOMAINS':
      return {...state, domains: action.domains};
    case 'SET_USER_WIDGETS':
      return {...state, widgets: action.widgets};
    default:
      return state
  }
}
