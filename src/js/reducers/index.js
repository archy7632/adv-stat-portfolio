import { combineReducers } from "redux"

import { stats } from "./reducers"
import { user } from "./user.js"
import { countries } from "./countries.js"
import { regions } from "./regions.js"
import { devices } from "./devices.js"

export default combineReducers({
  stats,
  user,
  countries,
  regions,
  devices,
})
