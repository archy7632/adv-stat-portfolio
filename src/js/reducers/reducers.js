import store from "../../store";
import { statsFetch } from "../actions/actions";

export function stats( state={
  viewRange: "7",
  startDate: null,
  stopDate: null,
  domain_id: null,
  widget_id: null,
  isLoading: null,
  errLoading: null,
  statsData: {}
}, action) {
  switch (action.type) {
    case 'SET_VIEW_RANGE':
      return {...state, viewRange: action.viewRange};
    case 'SET_VIEW_START_DATE':
      return {...state, startDate: action.startDate};
    case 'SET_VIEW_STOP_DATE':
      return {...state, stopDate: action.stopDate};
    case 'SET_DOMAIN_ID':
      return {...state, domain_id: action.domain_id};
    case 'SET_WIDGET_ID':
      return {...state, widget_id: action.widget_id};
    case 'STAT_DATA_FETCH_LOADING':
      return {...state, isLoading: action.isLoading};
    case 'STAT_DATA_FETCH_REQUEST_URL':
      return {...state, requestUrl: action.requestUrl};
    case 'STAT_DATA_FETCH_FAILURE':
      return {...state, errLoading: action.errLoading, errStatus: action.errStatus};
    case 'STAT_DATA_FETCH_SUCCESS':
      return {...state, statsData: action.statsData};
    default:
      return state
  }
}
