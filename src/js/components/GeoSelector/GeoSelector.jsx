import React, { PureComponent } from "react";
import style from "./style.scss";
import GeoRegionSelector from "../GeoRegionSelector/GeoRegionSelector.jsx";
import GeoCountrySelector from "../GeoCountrySelector/GeoCountrySelector.jsx";

import { Dropdown } from "semantic-ui-react";
import 'SemanticUI';

import { connect } from "react-redux";
import { statsFetch } from "../../actions/actions.js";
import { countriesFetch, setSelectedCountry } from "../../actions/countriesFetch.js";
import { regionsFetch } from "../../actions/regionsFetch.js";
import store from "../../../store.js";

@connect(store => ({
  countries: store.countries.countries,
  regions: store.regions.regions,
}))
export default class GeoSelector extends PureComponent {
  constructor(props) {
    super(props);
    this.state = ({
      countrySelectorValue: "null"
    })
  }

  render() {
    return (
      <div>
        <GeoCountrySelector
          handleStatRequest={this.props.handleStatRequest}
          />
          
        <GeoRegionSelector
          handleStatRequest={this.props.handleStatRequest}
          />
      </div>
    );
  }
}
