import React, { PureComponent } from 'react';
import style from './style.scss';

import { Dropdown } from "semantic-ui-react";
import 'SemanticUI';

import store from "../../../store.js";
import { connect } from "react-redux";
import { setDomain } from "../../actions/actions";

@connect(store => ({
  domains: store.user.domains
}))
export default class DomainSelector extends PureComponent {

  options = () => {
    const domains = this.props.domains;
    const parsedDomains = Object.keys(domains).map((item, index) => {
        return {
          "id": index,
          "value": domains[item].id,
          "text":  domains[item].domain
        }
      });
    return parsedDomains
  }

  handleChange = (e, data) => {
    let valueChanged = data.value !== this.props.dropdownDomainsValue;
    if (valueChanged) {
      this.props.dispatch(setDomain(data.value));
      this.props.handleStatRequest();
      this.props.handleDomainSelectorValueChange(data.value);
    }
  }


  render() {
    const DropdownDomains = () => (
      <Dropdown
        placeholder='всех доменов'
        className="dropdown-title"
        value={this.props.domainsSelectorValue}
        options={this.options()}
        onChange={this.handleChange}
        selection
        />
    )
    return <DropdownDomains />;
  }
}
