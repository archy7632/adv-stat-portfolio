import React, { Component, PureComponent } from "react";
import style from "./style.scss";

const AppStatusIndicator = (props) => {
  const { unauthorised, loading } = props;

  let title;
  let description;

  if (unauthorised) {
    title = "Вы не авторизованы"
    description = "Авторизуйтесь или зарегистрируйтесь"
  } else if (loading) {
    title = "Загрузка статистики..."
    description = ""
  }

    return (
      <div className="app-status-indicator">
        <div className="app-status-indicator__content-wrapper">
          <div className="app-status-indicator__title">{title}</div>
          <div className="app-status-indicator__description">{description}</div>
        </div>
        <div className="app-status-indicator__line"></div>
      </div>
    );
}
export default AppStatusIndicator
