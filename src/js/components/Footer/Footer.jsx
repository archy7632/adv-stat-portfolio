import React, { Component } from 'react';
import style from './style.scss';

import RelapLogo from "RelapLogo";

const Footer = () => 
  <div className="footer">
    <a href="/" className="footer__link footer__link--logo"><RelapLogo/></a>
    <a href="https://relap.io" className="footer__link">Личный кабинет</a>
    <a href="mailto:ads@relap.io" className="footer__link">Обратная связь</a>
  </div>

export default Footer
