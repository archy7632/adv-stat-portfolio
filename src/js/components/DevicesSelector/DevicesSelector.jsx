import React, { Component, PureComponent } from "react";
import style from "./style.scss";

import { Dropdown } from "semantic-ui-react";
import 'SemanticUI';

import { connect } from "react-redux";
import { setSelectedDevices } from "../../actions/devices.js";
import store from "../../../store.js";

@connect((store) => ({
  api: store.devices.api,
  is_mobile: store.devices.is_mobile,
}))
export default class DevicesSelector extends PureComponent {
  constructor(props) {
    super(props);
    this.state = ({
      devicesSelectorValue: "null"
    });
  }

  handleDevicesChange = (e, data) => {
    this.setState({
      devicesSelectorValue: data.value
    }, () => {
      this.props.dispatch(
        setSelectedDevices(
          this.devicesParse(data.value).api,
          this.devicesParse(data.value)["is_mobile"]
      ))
      this.props.handleStatRequest()
    });
    return data
  }

  devicesParse(value) {
    if (value === "null")
      return {
        "api": null ,
        "is_mobile": null
      }
    else if (value === "mobile")
      return {
        "api": "web",
        "is_mobile": true
      }
    else if (value === "apps")
      return {
        "api": "rest",
        "is_mobile": null
      }
    else if (value === "pc")
      return {
        "api": "web",
        "is_mobile": false
      }
    else if (value === "pcmobile")
      return {
        "api": "web",
        "is_mobile": null
      }
  }


  render() {
    let options = [
      {
        "id": 0,
        "text": "Все устройства",
        "value": "null",
      },
      {
        "id": 1,
        "text": "Десктопы и мобильные",
        "value": "pcmobile",
      },
      {
        "id": 2,
        "text": "Десктопы",
        "value": "pc",
      },
      {
        "id": 3,
        "text": "Мобильные",
        "value": "mobile",
      },
      {
        "id": 4,
        "text": "Приложения",
        "value": "apps",
      },
    ];

    const DevicesDropdown = (e, data) => (
      <Dropdown
        options={options}
        onChange={this.handleDevicesChange}
        value={this.state.devicesSelectorValue}
        selection
        />
    )

    return (<DevicesDropdown />);
  }
}
