import React, { Component } from 'react';
import style from './style.scss';
import PropTypes from 'prop-types'
import defaultIndexes from "Utils/theme.js"

import { groupNumber, isInArray } from "Utils";

const Table = (props) => {
  const { data, colorClass } = props;
  const totalNumbers = data;

  return (
    <div className={"summary-table-wrapper"}>
      {
        !totalNumbers
        ? null
        : Object.keys(defaultIndexes).map((item, index) => {

          if(isInArray(Object.keys(totalNumbers), item)) {
            return (
              <div key={index} className="summary-table__row">
                <div className="summary-table__box">
                  <span className="summary-table__cell__label">

                    {defaultIndexes[item]["eng"]}

                  </span>
                  <span className="summary-table__cell__stat-number">

                  { groupNumber(data[item]) }
                  { item === "revenue" ? "₽" : "" }

                  </span>
                </div>
              </div>
            )
          }
            return null
        })
      }
    </div>
  );
}
export default Table
