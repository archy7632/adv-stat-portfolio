import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { addZero, groupNumber } from "../../utils";
import { Bar, XAxis, BarChart } from 'recharts';
import style from './style.scss';
import Tooltip from "../ReCharts/Tooltip.js"
import CustomTooltip from "./CustomTooltip/CustomTooltip.jsx"
import CustomTick from "./CustomTick/CustomTick.jsx"
import Indicator from "./Indicator/Indicator.jsx"
import MainChartRows from "./MainChartRows.jsx"

import store from "../../../store.js";

@connect((store) => {
  return {
    isLoading: store.stats.isLoading,
    errLoading: store.stats.errLoading,
    viewRange: store.stats.viewRange,
  };
})
export default class MainChart extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      indicatorState: {
        noData: {
          title: "Нет данных",
          description: "Пороверьте правильно ли установлен виджет"
        },
        errLoading: {
          title: "Ошибка в получении статистики",
          description: "Кликните на ту же кнопку или перезагрузите страницу"
        },
        isLoading: {
          title: "Загрузка статистики...",
          description: ""
        }
      }
    }
  }

  renderIndicator = props => <Indicator {...props} />

  render() {
    const { data, indexes, barColors, isLoading, errLoading } = this.props;
    const barChartMargin = { top: 20, right: 5, bottom: 0, left: 5 };

    // console.log('data', data);
    // console.log('!data && this.props.isLoading',!data && this.props.isLoading);
    // console.log('this.props.isLoading',this.props.isLoading);
    // console.log('!data',!data);
    // console.log('this.props.errLoading',this.props.errLoading);

    // let isLoading = !data && this.props.isLoading;
    // let isErr = !data && isLoading && errLoading;

    let mainChartContent = this.renderIndicator(this.state.indicatorState.noData);

    if (isLoading) {
      mainChartContent = this.renderIndicator(this.state.indicatorState.isLoading);
    } else if (errLoading) {
      mainChartContent = this.renderIndicator(this.state.indicatorState.errLoading);
    } else if (!errLoading) {
      const dateInterval = this.props.viewRange > 14 ? 1 : 0; // tells reChart howmany dates skip of XAxis
      return (
        <MainChartRows
          data={data}
          indexes={indexes}
          viewRange={this.props.viewRange}/>
      );
    }

    return (
      <div className="graph-wrapper">
        {mainChartContent}
      </div>
    )
  }
}
