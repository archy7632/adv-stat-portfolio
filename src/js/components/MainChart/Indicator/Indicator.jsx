import React from 'react';
import style from './style.scss';

const MainChartIndicator = (props) => {
  const { title, description } = props;

  return (
    <div className="mainChart__indicator">
      <div className="mainChart__indicator__content-wrapper">
        <div className="mainChart__indicator__title">{title}</div>
        <div className="mainChart__indicator__description">{description}</div>
      </div>
      <div className="mainChart__indicator__line"></div>
    </div>
  );
}

export default MainChartIndicator
