import React from 'react';
import { groupNumber } from "Utils";
import defaultIndexes from "Utils/theme.js"

const CustomTooltip = (props) => {
  if (props.active) {
    const { payload, customLabel } = props;
    
    return (
      <div className="custom-tooltip">
        <p className="custom-tooltip__label">{`${defaultIndexes[customLabel]["eng"]}: ${groupNumber(payload[0].value)}`}</p>
      </div>
    );
  }
  return null;
};
export default CustomTooltip
