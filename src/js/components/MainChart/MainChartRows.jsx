import React, { PureComponent } from 'react';
import { connect } from "react-redux";
import { addZero, groupNumber } from "../../utils";
import { Bar, XAxis, BarChart } from 'recharts';
import style from './style.scss';
import Tooltip from "../ReCharts/Tooltip.js"
import CustomTooltip from "./CustomTooltip/CustomTooltip.jsx"
import CustomTick from "./CustomTick/CustomTick.jsx"
import Indicator from "./Indicator/Indicator.jsx"
import defaultIndexes from "Utils/theme.js"

import store from "../../../store.js";

function isInArray(data, item) {
  return data.some(value => value === item)
}

//Bars need to be rendered in a surtain order and quantity of rendered barCharts may vary
//thus map for every possible indexes of statistic
//and render bar only if there is data for the index
//indexes = WI, AI, eCPM etc.
const MainChartRows = (props) => {

  const { data, indexes, viewRange } = props;
  const dateInterval = viewRange > 14 ? 1 : 0; // tells reChart howmany dates skip of XAxis

  return (
    <div className="graph-wrapper">
      {
        Object.keys(defaultIndexes).map((item, index) => {
            let yAxisName = item;
            let barColor = defaultIndexes[item]["color"];
            if ( isInArray(Object.keys(data[0]), item) ) {//if there is a data for item
              return (
                <BarChart
                  key={index}
                  width={827}
                  height={item === 'revenue' ? 110 : 80}
                  data={data}
                  syncId='anyId'
                  margin={{ top: 20, right: 5, bottom: 0, left: 5 }} >

                  {
                    item === 'revenue'
                      ?<XAxis
                        tick={<CustomTick/>}
                        tickSize={6}
                        dataKey="date"
                        stroke="#e3e3e3"
                        strokeOpacity="1"
                        strokeWidth="2"
                        interval={dateInterval} />

                      :<XAxis
                        dataKey="nothing"
                        width={827}
                        height={1}
                        tickLine={false}
                        stroke="#e3e3e3"
                        strokeOpacity="1" />
                  }

                  <Tooltip
                    isAnimationActive={false}
                    cursor={{fill: "#f9fafb"}}
                    content={<CustomTooltip customLabel={item}/>} />

                  <Bar
                    barSize={32}
                    maxBarSize={32}
                    dataKey={yAxisName}
                    fill={barColor} />

                </BarChart>
              )
            } else {return null}
        })
      }
    </div>
  );
}

export default MainChartRows
