import React from 'react';
import { formatDateToDDMM } from "Utils";

const CustomTick = (props) => {
  if (props.hide) null
  const {x, y, stroke, payload} = props;

  return (
    <g transform={`translate(${x},${y})`}>
      <text className="custom-tick" x={0} y={0} dy={16} textAnchor="middle" fill="#979797">{formatDateToDDMM(payload.value)}</text>
    </g>
  );

}
export default CustomTick
