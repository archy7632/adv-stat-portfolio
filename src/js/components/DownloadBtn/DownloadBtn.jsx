import React, { Component } from 'react';
import style from './style.scss';

const DownloadBtn = (props) => {
  const { name, url } = props;
  return (<div className="download-btn__wrapper">
          <a href={url} className="download-btn__link">{name + ' '}</a>
        </div>)
}
export default DownloadBtn
