import React, { PureComponent } from 'react';
import style from './style.scss';
import PropTypes from 'prop-types';

import { Dropdown } from "semantic-ui-react";
import 'SemanticUI';

import RangeSelector from "../RangeSelector/RangeSelector.jsx";
import GeoSelector from "../GeoSelector/GeoSelector.jsx";
import DevicesSelector from "../DevicesSelector/DevicesSelector.jsx";
import DomainSelector from "../DomainSelector/DomainSelector.jsx";
import WidgetSelector from "../WidgetSelector/WidgetSelector.jsx";

// import { connect } from "react-redux";
// import { statsFetch, setFilterDomain, setFilterWidget } from "../../actions/actions";
// import store from "../../../store.js";

// @connect((store) => {
//   return {
//
//   };
// })
export default class Toolbar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      domainsSelectorValue: "null",
    }
  }

  handleDomainSelectorValueChange = domain_id => {
    this.setState({
      domainsSelectorValue: domain_id
    })
  }

  render() {
    const rangeSelectorOptions = [
      { "id": 0, "value": '7', "text": 'за 7 дней'},
      { "id": 1, "value": '14', "text": 'за 14 дней' },
      { "id": 2, "value": '31', "text": 'за 31 день' },
    ];

    return (
      <div className="filter ">
        <h1 className="page-title">
          Статистика по рекламе
          <DomainSelector
            domainsSelectorValue={this.state.domainsSelectorValue}
            handleDomainSelectorValueChange={this.handleDomainSelectorValueChange}
            handleStatRequest={this.props.handleStatRequest}
            widget_id={this.state.dropdownDomainsValue}
            widgetsDropdownOptions={this.widgetsDropdownOptions} />

          <RangeSelector
            handleStatRequest={this.props.handleStatRequest}
            options={rangeSelectorOptions} />
        </h1>
        <div className="flex-parent">

            <WidgetSelector
              domainsSelectorValue={this.state.domainsSelectorValue}
              handleStatRequest={this.props.handleStatRequest}
              options={this.state.widgetsOption} />

          <GeoSelector
            handleStatRequest={this.props.handleStatRequest}
            />
          <DevicesSelector
            handleStatRequest={this.props.handleStatRequest}
            domainsSelectorValue={this.state.domainsSelectorValue} />
        </div>


      </div>
    );

  }
}
