import React, { PureComponent } from 'react';
import style from './style.scss';
import PropTypes from 'prop-types';

import { Dropdown } from "semantic-ui-react";
import 'SemanticUI';

import { connect } from "react-redux";
import { setWidget } from "../../actions/actions";
import store from "../../../store.js";

@connect(store => ({
  widgets: store.user.widgets,
}))
export default class WidgetSelector extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      widgetsSelectorValue: "null",
    }
  }

  options = () => {
    const parseWidgets = this.props.widgets[this.props.domainsSelectorValue].map((item, index) => {
      return {
        "id": index + 1,
        "value": item.value,
        "text": item.text
      }
    });

    const noWidget = {
      "id": 0,
      "value": "null",
      "text": "Виджет не установлен"
    }

    return parseWidgets
  }


  handleWidgetChange = (e, data) => {
    let valueChanged = data.value !== this.state.currantWidget_id;
    if(valueChanged) {
      this.setState({
        widgetsSelectorValue: data.value,
      },() => {
        this.props.dispatch(setWidget(data.value));
        this.props.handleStatRequest();
      });
    }
  }


  render() {

    const DropdownWidgets = () => (
      <Dropdown
        disabled={this.props.domainsSelectorValue === "null"}
        placeholder='Все виджеты'
        value={this.state.widgetsSelectorValue}
        options={this.options()}
        onChange={this.handleWidgetChange}
        selection

        />
    )

    return <DropdownWidgets />;

  }
}
