import React, { Component } from 'react';
import style from './style.scss';
import PropTypes from 'prop-types'
// import { Select } from "relap-ui";
import { Dropdown } from 'semantic-ui-react';
import 'semantic-ui-css/semantic.min.css';
import { connect } from "react-redux";
import { dataFetchRequest, setFilterDomain, setFilterWidget } from "../../actions/actions";
import store from "../../../store.js";

@connect((store) => {
  return {
    domains: store.userReducer.user.company.domains,

    viewRange: store.statisticalData.viewRange,
    startDate: store.statisticalData.startDate,
    stopDate:  store.statisticalData.stopDate,
  };
})
export default class Filter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currantDomain_id: "null",
      currantWidget_id: "null",
      selectedDomain: {},
      widgetsList: [{"items": []}],

    };
  }

  handleDomainsSelectChange = (value, order_id, index, props ) => {
    this.setState({
      currantDomain_id: this.props.domains[order_id].id,
      currantWidget_id: "null",
      selectedDomain: this.props.domains[order_id] // thanks to this state we don't need to use order_id in other components
    }, () => {
      this.props.dispatch(
        dataFetchRequest("https://irassadindev.relap.io/personal/api/stat/monetization"
          + "?start=" + this.props.startDate
          + "&stop=" + this.props.stopDate
          + "&domains=" + [this.state.currantDomain_id]
          + "&widgets=" + [this.state.currantWidget_id]
        )
      )
      this.props.dispatch(setFilterDomain(this.state.currantDomain_id));
      this.props.dispatch(setFilterWidget(this.state.currantWidget_id));
      this.getWidgetList();
    });
  }

  handleWidgetsSelectChange = (id) => {
    this.setState({
      currantWidget_id: id
    }, () => {
      this.props.dispatch(
        dataFetchRequest("https://irassadindev.relap.io/personal/api/stat/monetization"
          + "?start=" + this.props.startDate
          + "&stop=" + this.props.stopDate
          + "&domains=" + [this.state.currantDomain_id]
          + "&widgets=" + [this.state.currantWidget_id]
        )
      )
      this.props.dispatch(setFilterWidget(this.state.currantWidget_id));
    });
  }

  getWidgetList = () => {
    const widgetsFixed = this.state.selectedDomain.widgets.map((item) => {
      return {
        "value": item.id,
        "label": item.name
      }
    });
    this.setState({
      widgetsList: [{"items": widgetsFixed}]
    })
  }

  render() {
    const domainsFixed = this.props.domains.map((item, index) => {
      return {
        "order_id": index,
        "value": item.id,
        "text": item.domain
      }
    });
    const domainsList =
    [ {
    key: 'Jenny Hess',
    text: 'Jenny Hess',
    value: 'Jenny Hess',
  },
  {
    key: 'Elliot Fu',
    text: 'Elliot Fu',
    value: 'Elliot Fu',
  },
  {
    key: 'Stevie Feliciano',
    text: 'Stevie Feliciano',
    value: 'Stevie Feliciano',
  },
  {
    key: 'Christian',
    text: 'Christian',
    value: 'Christian',
  },
  {
    key: 'Matt',
    text: 'Matt',
    value: 'Matt',
  },
  {
    key: 'Justen Kitsune',
    text: 'Justen Kitsune',
    value: 'Justen Kitsune',
  },
];

    const DropdownExampleSelection = () => (
      <Dropdown placeholder='Select Friend' fluid selection options={domainsList} />
    )
    return (
      <div className="filter flex-parent">
      <DropdownExampleSelection/>


      </div>
    );

  }
}
