import React, { Component, PureComponent } from "react";
import style from "./style.scss";

import { Dropdown } from "semantic-ui-react";
import 'SemanticUI';

import axios from "axios";

import { connect } from "react-redux";
import { setSelectedRegion } from "../../actions/regionsFetch.js";
import store from "../../../store.js";

@connect(store => ({
  regions: store.regions.regions,
}))
export default class GeoRegionSelector extends PureComponent {
  constructor(props) {
    super(props);
    this.state = ({
      regionSelectorValue:  "null"
    });
  }

  options() {
    const parsedRegions = this.props.regions.map((item, index) => {
      return {
          "key": item.code,
          "value": item.code,
          "text": item.name.ru
        };
    });

    const options = [{
        "key": 0,
        "value": "null",
        "text": "Все регионы"
      },
      ...parsedRegions ]

    return options
  }


  handleChange = (e, data) => {
    let valueChanged = data.value !== this.state.regionSelectorValue;
    if (valueChanged) {
      this.setState({
        regionSelectorValue: data.value,
      }, () => {
        this.props.dispatch(setSelectedRegion(data.value));
        this.props.handleStatRequest();
      });
    }
  }


  render() {
    const DropdownRegion = (e, data) => (
      <Dropdown
        value={this.state.regionSelectorValue}
        options={this.options()}
        onChange={this.handleChange}
        selection
        />
    )

    return (<DropdownRegion />);
  }
}
