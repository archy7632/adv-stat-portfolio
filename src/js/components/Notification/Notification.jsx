import React, { Component, PureComponent } from "react";
import style from "./style.scss";
import Tab from "../Tab/Tab.jsx";

import { connect } from "react-redux";
import { setViewRange } from "../../actions/actions.js";
import store from "../../../store.js";

@connect((store) => {
  return {
    viewRange: store.setViewRange
  };
})
export default class Notification extends Component {
  constructor(props) {
    super(props);
  }

  handleTabClick = (viewRange) => {
    this.props.dispatch(setViewRange(viewRange));
  }

  render() {
    const { items } = this.props;
    return (
      <div className="notification">
        <h2 className="notification__title"></h2>
        <span className="notification__description"></span>
        <tabel>
          <tbody>
          <th>Виджет</th>
          <th>Статус</th>
          <th>Реклама</th>
          <th>eCPM</th>
          {
            <tr>
            <td></td>
            <td></td>
            <td></td>
            </tr>
          }
          </tbody>
        </tabel>
      </div>
    );
  }
}
