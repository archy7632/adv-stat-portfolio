import React, { Component, PureComponent } from "react";
import style from "./style.scss";
import GeoRegionSelector from "../GeoRegionSelector/GeoRegionSelector.jsx";


import { Dropdown } from "semantic-ui-react";
import 'SemanticUI';

import { connect } from "react-redux";
import { statsFetch } from "../../actions/actions.js";
import { countriesFetch, setSelectedCountry } from "../../actions/countriesFetch.js";
import { regionsFetch } from "../../actions/regionsFetch.js";
import store from "../../../store.js";

@connect(store => ({
  countries: store.countries.countries
}))
export default class GeoCountrySelector extends PureComponent {
  constructor(props) {
    super(props);
    this.state = ({ countrySelectorValue: "null" })
  }

  handleChange = (e, data) => {
    let valueChanged = data.value !== this.state.countrySelectorValue;
    if (valueChanged) {
      this.setState({
        countrySelectorValue: data.value,
      }, () => {
        this.props.dispatch(regionsFetch(this.state.countrySelectorValue));
        this.props.dispatch(setSelectedCountry(data.value));
        this.props.handleStatRequest();
      });
    }
  }

  options = () => {
    const options = this.props.countries.map((item, index) => {
      return {
          "key": item.code,
          "value": item.code,
          "text": item.name.ru
        };
    });
    return [ {
      "key": 0,
      "value": "null",
      "text": "Все страны"
    }, ...options ]
  }

  render() {
    const DropdownCountry = () => (
      <Dropdown
        placeholder="Все страны"
        className="dropdown-country"
        value={this.state.countrySelectorValue}
        options={this.options()}
        onChange={this.handleChange}
        selection
        search
        />
    )

    return <DropdownCountry />;
  }
}
