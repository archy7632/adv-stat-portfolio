import React from "react";
import style from "./style.scss";
import Tab from "../Tab/Tab.jsx";

const Tabs = (props) => {
  const { items, viewRange, disabled} = props;

  const handleTabClick = (viewRange) => {
    props.handleTabClick(viewRange)
  }

  return (
    <div className={"tabs " + disabled}>
      <span className="tabs__preword">{items[0].preword}</span>
      <ul className="tabs__wrapper">

        {
          items.map(
            ( item, index ) => <Tab item={item} key={index} handleTabClick={handleTabClick} viewRange={viewRange} />
          )
        }
        
      </ul>
    </div>
  )
}
export default Tabs
