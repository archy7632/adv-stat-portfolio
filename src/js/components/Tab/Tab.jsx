import React from 'react';
import style from './style.scss';

const Tab = (props) => {
  const { item, viewRange } = props;
  const activeTab = viewRange === item.key ? "tabs__tab--active" : "" ;

  const handleTabClick = (item) => (e) => {
    props.handleTabClick(item)
  }

  return (
    <li className={"tabs__tab " + activeTab}
        onClick={handleTabClick(item.key)} >
      {item.name}
    </li>
  )
}
export default Tab
