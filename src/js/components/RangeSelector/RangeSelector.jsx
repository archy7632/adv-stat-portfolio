import React, { PureComponent } from "react";
import style from "./style.scss";

import { Dropdown } from "semantic-ui-react";
import 'SemanticUI';

import { connect } from "react-redux";
import { setViewRange } from "../../actions/actions.js";
import store from "../../../store.js";

import { getDate } from '../../utils';


@connect(store => ({
  viewRange: store.stats.viewRange,
}))
export default class RangeSelector extends PureComponent {

  handleChange = (e, data) => {
    let valueChanged = data.value !== this.props.viewRange;
    if (valueChanged) {
      this.props.dispatch(setViewRange(data.value));
      this.props.handleStatRequest();
    }
  }

  render() {
    const { items, options, viewRange } = this.props;

    const DropdownViewRange = () => (
      <Dropdown
        selection
        className="dropdown-title"
        options={options}
        value={this.props.viewRange}
        onChange={this.handleChange} />
    )

    return (

      <DropdownViewRange />

    );
  }
}
