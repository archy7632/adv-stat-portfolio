import { applyMiddleware, createStore } from "redux"
import {createLogger} from "redux-logger"
import thunk from "redux-thunk"
import promise from "redux-promise-middleware"

const logger = createLogger({ collapsed: true })

import reducer from "./js/reducers/"
import { statDataRequest, statDataIsLoading, statDataLoadingFailure, statDataLoadingSuccess, setViewRange } from "./js/actions/actions"

const middleware = ENV === "development" ? applyMiddleware( promise(), thunk, logger ) : applyMiddleware( promise(), thunk );


export default createStore(reducer, middleware)
